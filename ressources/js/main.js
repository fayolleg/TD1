/**
 * @param {HTMLElement} button La balise <button> cliquée
 */


let buttons = document.querySelectorAll(".delete-feedy");
buttons.forEach(button => {
    button.addEventListener("click", function() {
        supprimerPublication(this);
    });
});

function supprimerPublication(button) {
    let idPublication = button.dataset.idPublication;
    let URL = apiBase + "publications/" + idPublication;

    fetch(URL, {method: "DELETE"})
        .then(response => {
            if (response.status === 204) {
                // Plus proche ancêtre <div class="feedy">
                let divFeedy = button.closest("div.feedy");
                divFeedy.remove();
            }
        });
}

function templatePublication(publication, utilisateur) {
    return `<div class="feedy">
   <div class="feedy-header">
      <a href="${pagePersoBase + publication.auteur.idUtilisateur}">
            <img alt="profile picture" src="${imgBase}/utilisateurs/${utilisateur.nomPhotoDeProfil}" class="avatar">
      </a>
      <div class="feedy-info">
            <button class="delete-feedy" data-id-publication="${publication.idPublication}" onclick="supprimerPublication(this)">Supprimer</button>
            <span>${utilisateur.login}</span><span> - </span><span>${publication.date}</span>
            <p>${publication.message}</p>
      </div>
   </div>
</div>`;
}

// if ("content" in document.createElement("template")) {
//     const template = document.getElementById("template");
//     if(template !== null) {
//        const clone = document.importNode(template.content, true);
//        const bouton = document.getElementById("feedy-new-submit");
//        bouton.addEventListener("click", soumettrePublication);
//        template.appendChild(clone)
//     }
// }


async function soumettrePublication() {
    const messageElement = document.getElementById('message')
    // On récupère le message
    let message = messageElement.value;
    // On vide le formulaire
    messageElement.value = "";
    // On utilise la variable globale apiBase définie dans base.html.twig
    let URL = apiBase + "publications";

    let response = await fetch(URL, {
        method: "POST",
        body: JSON.stringify({message: escapeHtml(message)}),

        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json; charset=UTF-8',
        },
    });
    if (response.status !== 200)
        // (Hors TD) Il faudrait traiter l'erreur
        return;
    let publication = await response.json();
    // Utilisateur par défaut en attendant la suite
    let auteur = await fetch(apiBase + "utilisateurs/" + publication.auteur.idUtilisateur);
    let utilisateur = await auteur.json();
    let formElement = document.getElementById("feedy-new");
    formElement.insertAdjacentHTML('afterend', templatePublication(publication, utilisateur));
}

document.getElementById("feedy-new-submit").addEventListener("click", () => soumettrePublication());

function escapeHtml(text) {
    // https://stackoverflow.com/questions/1787322/what-is-the-htmlspecialchars-equivalent-in-javascript
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}
