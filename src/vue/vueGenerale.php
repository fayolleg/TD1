<?php

use TheFeed\Lib\ConnexionUtilisateurSession;

use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\Conteneur;

/** @var UrlGenerator $generateurUrl */
$generateurUrl = Conteneur::recupererService("generateurUrl");
/** @var UrlHelper $assistantUrl */
$assistantUrl = Conteneur::recupererService("assistantUrl");

/**
 * @var string $pagetitle
 * @var string $cheminVueBody
 * @var String[][] $messagesFlash
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?= $pagetitle ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo $assistantUrl->getAbsoluteUrl('../ressources/css/styles.css')  ?>">
</head>

<body>
    <header>
        <div id="titre" class="center">
            <a href="<?php echo $generateurUrl->generate('afficherListe') ?>"><span>The Feed</span></a>
            <nav>
                <a href="<?php echo $generateurUrl->generate('afficherListe') ?>">Accueil</a>
                <?php
                if (!ConnexionUtilisateurSession::estConnecte()) {
                ?>
                    <a href="<?php echo $generateurUrl->generate('afficherFormulaireCreation') ?>">Inscription</a>
                    <a href="<?php echo $generateurUrl->generate('afficherFormulaireConnexion') ?>">Connexion</a>
                <?php
                } else {
                    $idUtilisateurURL = rawurlencode(ConnexionUtilisateurSession::getIdUtilisateurConnecte());
                ?>
                    <a href="<?php echo $generateurUrl->generate('afficherPublications', ["idUtilisateur" => $idUtilisateurURL]) ?>">Ma
                        page</a>
                    <a href="<?php echo $generateurUrl->generate('deconnecter') ?>">Déconnexion</a>
                <?php } ?>
            </nav>
        </div>
    </header>
    <div id="flashes-container">
        <?php
        foreach (["success", "error"] as $type) {
            foreach ($messagesFlash[$type] as $messageFlash) {
        ?>
                <span class="flashes flashes-<?= $type ?>"><?= $messageFlash ?></span>
        <?php
            }
        }
        ?>
    </div>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</body>

</html>