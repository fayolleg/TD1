<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateurInterface;
use TheFeed\Lib\ConnexionUtilisateurSession;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\Exception\ServiceException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ControleurPublicationAPI extends ControleurGenerique
{

    public function __construct (
        ContainerInterface $container,
        private readonly PublicationServiceInterface $publicationService,
        private ConnexionUtilisateurInterface $connexionUtilisateur
    )
    {
        parent::__construct($container);
    }

    #[Route(path: '/api/publications/{idPublication}', name:'supprimerPublication', methods:["DELETE"])]
    public function supprimer($idPublication): Response
    {
        try {
            $idUtilisateurConnecte = ConnexionUtilisateurSession::getIdUtilisateurConnecte();
            $this->publicationService->supprimerPublication($idPublication, $idUtilisateurConnecte);
            return new JsonResponse('', Response::HTTP_OK);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }

    #[Route(path: '/api/publications/{idPublication}', name:'afficherDetailPublication', methods:["GET"])]
    public function afficherDetailPublication($idPublication): Response{
        try{
            $publication = $this->publicationService->recupererPublicationParId($idPublication, false);
            return new JsonResponse($publication, Response::HTTP_OK);
        }catch (\Exception $e){
            return new JsonResponse(["error" => $e->getMessage()], $e->getCode());
        }
    }

    #[Route(path: '/api/publications', name:'afficherListePublication', methods:["GET"])]
    public function afficherListePublication(): Response{
        try{
            $publication = $this->publicationService->recupererPublications();
            return new JsonResponse($publication, Response::HTTP_OK);
        }catch (\Exception $e){
            return new JsonResponse(["error" => $e->getMessage()], $e->getCode());
        }
    }

    #[Route(path: '/api/publications', name:'posterPublication', methods:["POST"])]
    public function posterPublication(Request $request): Response
    {
        try {
            $corps = json_decode($request->getContent());
            $message = $corps->message ?? null;
            $idUtilisateurConnecte = ConnexionUtilisateurSession::getIdUtilisateurConnecte();
            $publication = $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
            return new JsonResponse($publication, Response::HTTP_OK);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }catch (JsonException $exception) {
            return new JsonResponse(
                ["error" => "Corps de la requête mal formé"],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

}
