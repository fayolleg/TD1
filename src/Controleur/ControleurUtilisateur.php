<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateurInterface;
use TheFeed\Lib\ConnexionUtilisateurSession;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurUtilisateur extends ControleurGenerique
{

    public function __construct(ContainerInterface $container,private UtilisateurServiceInterface $utilisateurServiceInterface, private PublicationServiceInterface $publicationServiceInterface,
                                private readonly ConnexionUtilisateurInterface $connexionUtilisateurSession,
                                private readonly ConnexionUtilisateurInterface $connexionUtilisateurJWT,)
    {parent::__construct($container);}
    


    public function afficherErreur($messageErreur = "", $controleur = ""): \Symfony\Component\HttpFoundation\Response
    {
        return parent::afficherErreur($messageErreur, "utilisateur");
    }

    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublications', methods:["GET"])]
    public function afficherPublications($idUtilisateur): \Symfony\Component\HttpFoundation\Response
    {
        $login = "";
        try{
            $utilisateur = $this->utilisateurServiceInterface->recupererUtilisateurParId($idUtilisateur, false);
            $login = htmlspecialchars($utilisateur->getLogin());
        }catch (\Exception $e){
            MessageFlash::ajouter("error", "Login inconnu.");
            return $this->rediriger("publication");
        }

        $publications = $this->publicationServiceInterface->recupererPublicationsUtilisateur($idUtilisateur);
        return $this->afficherTwig('utilisateur/page_perso.html.twig', [
            "publications" => $publications, "login" => $login
        ]);
    }

    #[Route(path: '/inscription', name:'afficherFormulaireCreation', methods:["GET"])]
    public function afficherFormulaireCreation(): \Symfony\Component\HttpFoundation\Response
    {
        return $this->afficherTwig('utilisateur/inscription.html.twig', []);
    }

    #[Route(path: '/inscription', name:'creerDepuisFormulaire', methods:["POST"])]
    public function creerDepuisFormulaire(): \Symfony\Component\HttpFoundation\RedirectResponse
    {

            $login = $_POST['login'] ?? null;
            $motDePasse = $_POST['mot-de-passe'] ?? null;
            $adresseMail = $_POST['email'] ?? null;
            $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;

            try {
                $this->utilisateurServiceInterface->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
                MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
                return $this->rediriger("afficherFormulaireConnexion");
            } catch (\Exception $e) {
                MessageFlash::ajouter("error", $e->getMessage());
                return $this->rediriger("afficherFormulaireCreation");
            }

    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): \Symfony\Component\HttpFoundation\Response
    {
        return $this->afficherTwig('utilisateur/connexion.html.twig', []);
    }


    #[Route(path: '/connexion', name:'traiterConnexion', methods:["POST"])]
    public function connecter(): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        if (!(isset($_POST['login']) && isset($_POST['mot-de-passe']))) {
            MessageFlash::ajouter("error", "Login ou mot de passe manquant.");
            return $this->rediriger("afficherFormulaireConnexion");
        }
        try{
            $idutilisateur = $this->utilisateurServiceInterface->verifierIdentifiantUtilisateur($_POST['login'], $_POST['mot-de-passe']);
            $this->connexionUtilisateurSession->connecter($idutilisateur);
            $this->connexionUtilisateurJWT->connecter($idutilisateur);
            MessageFlash::ajouter("success", "L'utilisateur a bien été connecté.");
            return $this->rediriger("afficherListe");
        }catch (\Exception $e){
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherFormulaireConnexion");
        }
    }

    #[Route(path: '/deconnexion', name:'deconnecter', methods:["GET"])]
    public function deconnecter(): Response
    {
        if (!$this->connexionUtilisateurSession->estConnecte()) {
            MessageFlash::ajouter("error", "Utilisateur non connecté.");
            return ControleurPublication::rediriger('afficherListe');
        }
        $this->connexionUtilisateurSession->deconnecter();
        $this->connexionUtilisateurJWT->deconnecter();
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return ControleurUtilisateur::rediriger('afficherListe');
    }
}
