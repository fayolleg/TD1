<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurGenerique {
    public function __construct(private ContainerInterface $container)
    {}

    protected function afficherVue(string $cheminVue, array $parametres = []): Response
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new Response($corpsReponse);
    }


    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    protected function rediriger(string $route, array $arg = []) : RedirectResponse
    {
        $url = $this->container->get("url_generator")->generate($route, $arg);
        return new RedirectResponse($url);

    }

    public function afficherErreur($messageErreur = "", $statusCode = 400): Response
    {
        $reponse = $this->afficherTwig('erreur.html.twig', [
            "messageErreur" => $messageErreur,
        ]);

        $reponse->setStatusCode($statusCode);
        return $reponse;
    }

    public function afficherTwig(string $cheminVue, array $parametres = []): Response
    {
        $twig = $this->container->get("twig");
        $corpsReponse = $twig->render($cheminVue, $parametres);
        return new Response($corpsReponse);
    }

}