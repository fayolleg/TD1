<?php

namespace TheFeed\Controleur;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Loader\AttributeDirectoryLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use TheFeed\Lib\AttributeRouteControllerLoader;
use TheFeed\Lib\ConnexionUtilisateurSession;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;
use TheFeed\Controleur\ControleurPublication;
use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\PublicationService;
use TheFeed\Service\UtilisateurService;
use TheFeed\Configuration\ConfigurationBDDMySQL;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;


class RouteurURL
{


    /**
     * @throws \Exception
     */
    public static function traiterRequete(Request $requete): Response
    {
        $conteneur = new ContainerBuilder();
        $conteneur->set('container', $conteneur);
        $conteneur->setParameter('project_root', __DIR__ . '/../..');

        $loader = new YamlFileLoader($conteneur, new FileLocator(__DIR__ . "/../Configuration"));
        //On remplit le conteneur avec les données fournies dans le fichier de configuration
        $loader->load("conteneur.yml");

        $fileLocator = new FileLocator(__DIR__);
        $attrClassLoader = new AttributeRouteControllerLoader();
        $routes = (new AttributeDIRectoryLoader($fileLocator, $attrClassLoader))->load(__DIR__);
        //Après que les routes soient récupérées
        $conteneur->set('routes', $routes);


        $twig = $conteneur->get('twig');
        //Après l'instanciation de l'objet $request
        $conteneur->set('request_context', (new RequestContext())->fromRequest($requete));


//        $contexteRequete = (new RequestContext())->fromRequest($requete);
//        $generateurUrl = new UrlGenerator($routes, $contexteRequete);
//        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);
        $contexteRequete = $conteneur->get('request_context');
        $generateurUrl = $conteneur->get('url_generator');
        $assistantUrl = $conteneur->get('url_helper');

        $twig->addFunction(new TwigFunction("route", $generateurUrl->generate(...)));
        $twig->addFunction(new TwigFunction("asset", $assistantUrl->getAbsoluteUrl(...)));
        $twig->addGlobal('connexionUtilisateur', new ConnexionUtilisateurSession());
        $twig->addGlobal('messagesFlash', new MessageFlash());
//        Conteneur::ajouterService("generateurUrl", $generateurUrl);
//        Conteneur::ajouterService("assistantUrl", $assistantUrl);


        try {
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ContainerControllerResolver($conteneur);
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $reponse = call_user_func_array($controleur, $arguments);
        } catch (ResourceNotFoundException $exception) {
            $reponse = $conteneur->get('controller_generique')->afficherErreur($exception->getMessage(), 404);
        } catch (MethodNotAllowedException $exception) {
            $reponse = $conteneur->get('controller_generique')->afficherErreur($exception->getMessage(), 405);
        } catch (\Exception $exception) {
            $reponse = $conteneur->get('controller_generique')->afficherErreur($exception->getMessage());
        }
        return $reponse;

    }

//        $twigLoader = new FilesystemLoader(__DIR__ . '/../vue/');
//        $twig = new Environment(
//            $twigLoader,
//            [
//                'autoescape' => 'html',
//                'strict_variables' => true
//            ]
//        );
//        Conteneur::ajouterService("twig", $twig);

//        $conteneur->register('configuration_bdd_my_sql', ConfigurationBDDMySQL::class);
//
//        $connexionBaseService = $conteneur->register('connexion_base_de_donnees', ConnexionBaseDeDonnees::class);
//        $connexionBaseService->setArguments([new Reference('configuration_bdd_my_sql')]);
//
//        $publicationsRepositoryService = $conteneur->register('publication_repository',PublicationRepository::class);
//        $publicationsRepositoryService->setArguments([new Reference('connexion_base_de_donnees')]);
//
//        $utilisateurRepositoryService = $conteneur->register('utilisateur_repository',UtilisateurRepository::class);
//        $utilisateurRepositoryService->setArguments([new Reference('connexion_base_de_donnees')]);
//
//        $publicationService = $conteneur->register('publication_service', PublicationService::class);
//        $publicationService->setArguments([new Reference('utilisateur_repository'), new Reference('publication_repository')]);
//
//        $publicationControleurService = $conteneur->register('controleur_publication',ControleurPublication::class);
//        $publicationControleurService->setArguments([new Reference('publication_service')]);
//
//        $utilisateutService = $conteneur->register('utilisateur_service', UtilisateurService::class);
//        $utilisateutService->setArguments([new Reference('utilisateur_repository')]);
//
//        $utilisateurControleurService = $conteneur->register('controleur_utilisateur',ConnexionUtilisateurSession::class);
//        $utilisateurControleurService->setArguments([new Reference('utilisateur_service'), new Reference('publication_service')]);


//        $requete = Request::createFromGlobals();
//        $routes = new RouteCollection();
//
//        // Route afficherListe
//        $route = new Route("/publications", [
//            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
//        ]);
//        $route->setMethods(["GET"]);
//        $routes->add("afficherListe", $route);
//
//        // Route pour créer une publication
//        $routeCreation = new Route("/publications", [
//            "_controller" => "\TheFeed\Controleur\ControleurPublication::creerDepuisFormulaire",
//        ]);
//        $routeCreation->setMethods(["POST"]);
//        $routes->add("creerFeedDepuisFormulaire", $routeCreation);
//
//        // Route afficherListe pour connecté
//        $routeUneFoisCo = new Route("/", [
//            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
//        ]);
//        $routes->add("afficherListeCo", $routeUneFoisCo);
//
//        // Route afficher la page de connexion
//        $routeCoFormulaire = new Route("/connexion", [
//            "_controller" => "\TheFeed\Controleur\ConnexionUtilisateurSession::afficherFormulaireConnexion",
//
//        ]);
//        $routeCoFormulaire->setMethods(["GET"]);
//        $routes->add("afficherFormulaireConnexion", $routeCoFormulaire);
//
//        // Route traiter la connexion
//        $routeCoTraitement = new Route("/connexion", [
//            "_controller" => "\TheFeed\Controleur\ConnexionUtilisateurSession::connecter",
//        ]);
//        $routeCoTraitement->setMethods(["POST"]);
//        $routes->add("traiterConnexion", $routeCoTraitement);
//
//        // Route afficher la page de déconnexion
//        $routeDeconnexion = new Route("/deconnexion", [
//            "_controller" => "\TheFeed\Controleur\ConnexionUtilisateurSession::deconnecter",
//        ]);
//        $routeDeconnexion->setMethods(["GET"]);
//        $routes->add("deconnecter", $routeDeconnexion);
//
//
//        // Route pour afficher le formulaire de création d'un utilisateur
//        $routeCreationUtilisateur = new Route("/inscription", [
//            "_controller" => "\TheFeed\Controleur\ConnexionUtilisateurSession::afficherFormulaireCreation",
//        ]);
//        $routeCreationUtilisateur->setMethods(["GET"]);
//        $routes->add("afficherFormulaireCreation", $routeCreationUtilisateur);
//
//        // Route pour créer un utilisateur
//            $routeCreationUtilisateurTraitement = new Route("/inscription", [
//            "_controller" => "\TheFeed\Controleur\ConnexionUtilisateurSession::creerDepuisFormulaire",
//        ]);
//        $routeCreationUtilisateurTraitement->setMethods(["POST"]);
//        $routes->add("creerDepuisFormulaire", $routeCreationUtilisateurTraitement);
//
//        // Route pour voir les publications d'un utilisateur
//        $utilisateurPublications = new Route("/utilisateurs/{idUtilisateur}/publications", [
//            "_controller" => "\TheFeed\Controleur\ConnexionUtilisateurSession::afficherPublications",
//        ]);
//        $utilisateurPublications->setMethods(["GET"]);
//        $routes->add("afficherPublications", $utilisateurPublications);
//        $contexteRequete = (new RequestContext())->fromRequest($requete);

    // pour match :
    // NoConfigurationException If no routing configuration could be found
    // ResourceNotFoundException If the resource could not be found
    // MethodNotAllowedException If the resource was found but the request method is not allowed

    // pour getControleur:
    // LogicException If a controller was found based on the request but it is not callable

    // pour getArguments:
    // RuntimeException When no value could be provided for a required argument


}