<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateurInterface;
use TheFeed\Lib\connexionUtilisateurSession;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurPublication extends ControleurGenerique
{

    public function __construct(private ContainerInterface $container, private PublicationServiceInterface $publicationServiceInterface,
                                private ConnexionUtilisateurInterface $connexionUtilisateur)
    {
        parent::__construct($this->container);
    }

    #[Route(path: '/publications', name: 'afficherListe', methods: ["GET"])]
    #[Route(path: '/', name: 'afficherListeCo', methods: ["GET"])]
    public function afficherListe()
    {
        $publications = $this->publicationServiceInterface->recupererPublications();
        return $this->afficherTwig('publication/feed.html.twig', [
            "publications" => $publications,
        ]);
    }

    #[Route(path: '/publications', name: 'creerFeedDepuisFormulaire', methods: ["POST"])]
    public function creerDepuisFormulaire()
    {
        $idUtilisateurConnecte = $this->connexionUtilisateurSession->getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            $this->publicationServiceInterface->creerPublication($idUtilisateurConnecte, $message);
        } catch (ServiceException $e) {
            MessageFlash::ajouter('error', $e->getMessage());
        }

        return $this->rediriger('afficherListe');
    }


}