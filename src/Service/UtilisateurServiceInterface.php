<?php

namespace TheFeed\Service;

interface UtilisateurServiceInterface
{
    public function creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true);

    public function verifierIdentifiantUtilisateur($login, $mdp): int;

}