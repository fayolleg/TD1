<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\FileMovingServiceInterface;
use TheFeed\Service\UtilisateurService;

class UtilisateurServiceTest extends TestCase
{

    private $service;

    private $utilisateurRepositoryMock;

    //Dossier où seront déplacés les fichiers pendant les tests
    private $dossierPhotoDeProfil = __DIR__ . "/tmp/";

    private FileMovingServiceInterface $fileMovingService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->fileMovingService = new TestFileMovingService();
        mkdir($this->dossierPhotoDeProfil);
        $this->service = new UtilisateurService($this->utilisateurRepositoryMock, $this->dossierPhotoDeProfil, $this->fileMovingService);
    }

    public function testCreerUtilisateurPhotoDeProfil()
    {
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn(null);
        $this->utilisateurRepositoryMock->method("recupererParEmail")->willReturn(null);
        $this->utilisateurRepositoryMock->method("ajouter")->willReturnCallback(function ($utilisateur) {
            /* TODO : Tester l'existence du fichier (et eventuellement d'autres tests) */
            $this->assertFileExists($this->dossierPhotoDeProfil . $utilisateur->getNomPhotoDeProfil());

        });
        $this->service->creerUtilisateur("test", "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
    }

    public function testDeconnecter(){
        $this->expectException(ServiceException::class);
        $this->service->deconnecter();
    }


    public function testeCreerUtilisateurSansLogin()
    {
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        // on vérifie aue ça renvoie une ServiceException
        $this->expectException(ServiceException::class);
        $this->service->creerUtilisateur(null, "TestMdp123", "test@example.com", $donneesPhotoDeProfil);

    }

    public function testeCreerUtilisateurMailIncorrect(){
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        // on vérifie aue ça renvoie une ServiceException
        $this->expectException(ServiceException::class);
        $this->service->creerUtilisateur("test", "TestMdp123", "testexample.com", $donneesPhotoDeProfil);
    }

    public function testRecupererparidFaux(){
        $this->expectException(ServiceException::class);
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        $this->service->creerUtilisateur("test", "TestMdp123", "testexampl@ea.com", $donneesPhotoDeProfil);
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(null);
        $this->service->recupererUtilisateurParId(6, false);
    }

    public function testRecupererparidBon(){
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        $this->service->creerUtilisateur("test", "TestMdp123", "testexampl@ea.com", $donneesPhotoDeProfil);
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());
        self::assertEquals(new Utilisateur(), $this->service->recupererUtilisateurParId(1));
    }


    protected function tearDown(): void
    {
        //Nettoyage
        parent::tearDown();
        foreach (scandir($this->dossierPhotoDeProfil) as $file) {
            if ('.' === $file || '..' === $file) continue;
            unlink($this->dossierPhotoDeProfil . $file);
        }
        rmdir($this->dossierPhotoDeProfil);
    }

}
