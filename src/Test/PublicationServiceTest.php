<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\PublicationService;

class PublicationServiceTest extends TestCase
{


    private $service;

    private $publicationRepositoryMock;

    private $utilisateurRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepositoryInterface::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->service = new PublicationService($this->utilisateurRepositoryMock,$this->publicationRepositoryMock);
    }

//    public function testNombrePublications() {
//        //Fausses publications, vides
//        $fakePublications = [new Publication(), new Publication()];
//        //On configure notre faux repository pour qu'il renvoie nos publications définies ci-dessus
//        $this->publicationRepositoryMock->method("recuperer")->willReturn($fakePublications);
//        //Test
//        $this->assertCount(2, $this->service->recupererPublications());
//    }

     public function testCreerPublicationUtilisateurInexistant(){
         $this->expectExceptionMessage("Il faut être connecté pour publier un feed");
         $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(null);
         $this->service->creerPublication(-1, "uughtfj");
     }

    public function testCreerPublicationVide(){
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(Utilisateur::create(1, "login", "email", "motDePasse"));
        $this->expectExceptionMessage("Le message ne peut pas être vide!");
        $this->service->creerPublication(1, "");
    }

    public function testCreerPublicationTropGrande(){
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(Utilisateur::create(1, "login", "email", "motDePasse"));
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->service->creerPublication(1, str_repeat("coucouuuuu", 100));
    }

    public function testNombrePublications(){
        $this->publicationRepositoryMock->method("recuperer")->willReturn([1,2,3,4,5,6]);
        self::assertEquals(6, sizeof($this->service->recupererPublications()));
    }

    public function testNombrePublicationsUtilisateur(){
        $this->publicationRepositoryMock->method("recupererParAuteur")->willReturn([1,2,3]);
        self::assertEquals(3, sizeof($this->service->recupererPublicationsUtilisateur(4)));
    }

    public function testNombrePublicationsUtilisateurInexistant(){
        $this->publicationRepositoryMock->method("recupererParAuteur")->willReturn([]);
        self::assertEquals(0, sizeof($this->service->recupererPublicationsUtilisateur(-1)));
    }

    public function testCreerPublicationValide()
    {
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(Utilisateur::create(1, "login", "email", "motDePasse"));
        $this->publicationRepositoryMock->method("ajouter")->willReturnCallback(function ($publication){
            $this->publicationRepositoryMock->method("recuperer")->willReturn([$publication]);
        });
        $this->service->creerPublication(1, "coucou");
        $this->assertEquals(1, sizeof($this->service->recupererPublications()));
    }


}