<?php

////////////////////
// Initialisation //
////////////////////
///
//use TheFeed\Lib\Psr4AutoloaderClass;
//
//require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
//
//
//// initialisation en désactivant l'affichage de débogage
//$chargeurDeClasse = new Psr4AutoloaderClass(false);
//$chargeurDeClasse->register();
//// enregistrement d'une association "espace de nom" → "dossier"
//$chargeurDeClasse->addNamespace('TheFeed', __DIR__ . '/../src');

use Symfony\Component\HttpFoundation\Request;
use TheFeed\Controleur\RouteurURL;

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once __DIR__ . '/../vendor/autoload.php';

/////////////
// Routage //
/////////////

$requete = Request::createFromGlobals();
try {
    RouteurURL::traiterRequete($requete)->send();
} catch (Exception $e) {
    echo "Erreur : " . $e->getMessage();
}
